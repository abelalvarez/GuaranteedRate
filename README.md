# GuaranteedRate

## Assumptions

The file being uploaded has no headers row, a flag can be added to have more flexibility. Currently only one type of file can be uploaded, we could enhance it by adding generics or different parsing mechanism.

### Parser.Cli

Added the option to decide if you want the data sorted asc or desc. Showing all data stored in the DB, we could add to only show N number of records by adding a new parameter.

### Parser.WebApi

Added query parameter to sorting endpoints to sort asc or desc

# Usage Cli

`help`
```
Input
ParserCli.exe --help

Output
ParserCli 1.0.0
Copyright (C) 2021 ParserCli

  -p, --path          (Default: ) File Path to analyze.

  -s, --sort          (Default: gender) Sort column

  -d, --descending    (Default: false) Sort descending

  --help              Display this help screen.

  --version           Display version information.
```
``-d, --descending``

```
Input
ParserCli.exe --version

```

``-p, --path``

```
Input
ParserCli.exe -p C:\{filepath}
ParserCli.exe --path C:\{filepath}

```

``-s, --sort``

```
Input
ParserCli.exe -s gender
ParserCli.exe --sort gender

```


``-d, --descending``

```
Input
ParserCli.exe -d
ParserCli.exe --descending

```

# Usage WebApi

### Endpoints
```
[POST] /records
[GET]  /records/gender
[GET]  /records/birthday
[GET]  /records/name
```
