﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Parser.Core;
using Parser.Core.Dtos;
using System;
using System.Collections.Generic;

namespace Parser.WebApi.Controllers
{
    [ApiController]
    [Route("records")]
    public class ParserController : ControllerBase
    {
        private readonly ILogger<ParserController> _logger;
        private readonly IParserSorterService _parserSorterService;

        public ParserController(ILogger<ParserController> logger, IParserSorterService parserSorterService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _parserSorterService = parserSorterService ?? throw new ArgumentNullException(nameof(parserSorterService)); ;
        }

        [HttpPost]
        public IActionResult SaveRecords(string input)
        {
            return Created("records", _parserSorterService.ParseAndSave(input));
        }

        [HttpGet("gender")]
        public ActionResult<List<Person>> SortByGender(bool descending = false)
        {
            var sortConfig = new SortOptions { ByColumn = "Gender", Descending = descending };
            var results = _parserSorterService.Sort(sortConfig);

            return Ok(results);
        }

        [HttpGet("birthday")]
        public ActionResult<List<Person>> SortByBirthday(bool descending = false)
        {
            var sortConfig = new SortOptions { ByColumn = "DateOfBirth", Descending = descending };
            var results = _parserSorterService.Sort(sortConfig);

            return Ok(results);
        }

        [HttpGet("name")]
        public ActionResult<List<Person>> SortByName(bool descending = false)
        {
            var sortConfig = new SortOptions { ByColumn = "firstname", Descending = descending };
            var results = _parserSorterService.Sort(sortConfig);

            return Ok(results);
        }
    }
}
