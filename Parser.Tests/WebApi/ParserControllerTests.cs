﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Parser.Core;
using Parser.Core.Dtos;
using Parser.WebApi.Controllers;
using System;
using System.Collections.Generic;

namespace Parser.Tests.WebApi
{
    [TestClass]
    public class ParserControllerTests
    {
        private ParserController _parserController;
        private readonly Mock<ILogger<ParserController>> _logger = new Mock<ILogger<ParserController>>();
        private readonly Mock<IParserSorterService> _parserSorterService = new Mock<IParserSorterService>();

        public ParserControllerTests()
        {
            _parserController = new ParserController(_logger.Object, _parserSorterService.Object);
        }

        #region Constructor
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ParserController_NullLogger_ArgumentNullException()
        {
            _parserController = new ParserController(null, _parserSorterService.Object);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ParserController_NullParserSorterService_ArgumentNullException()
        {
            _parserController = new ParserController(_logger.Object, null);
        }
        #endregion

        [TestMethod]
        public void SaveRecords_Parameter_CallParseAndSave()
        {
            _parserController.SaveRecords("");
            _parserSorterService.Verify(x => x.ParseAndSave(It.Is<string>(x => x == "")), Times.Once);
        }

        [TestMethod]
        public void SortByGender_DescendingFalse_CallSort()
        {
            _parserSorterService.Setup(x => x.Sort(It.IsAny<SortOptions>()))
                .Returns(new List<Person>() { new Person() });

            var response = _parserController.SortByGender();
            var results = response.Result as OkObjectResult;

            _parserSorterService.Verify(x => x.Sort(It.Is<SortOptions>(x => x.ByColumn == "Gender" && x.Descending == false)), Times.Once);
            Assert.AreEqual(1, ((List<Person>)results.Value).Count);
        }

        [TestMethod]
        public void SortByGender_DescendingTrue_CallSort()
        {
            _parserSorterService.Setup(x => x.Sort(It.IsAny<SortOptions>()))
                .Returns(new List<Person>() { new Person() });

            var response = _parserController.SortByGender(true);
            var results = response.Result as OkObjectResult;

            _parserSorterService.Verify(x => x.Sort(It.Is<SortOptions>(x => x.ByColumn == "Gender" && x.Descending == true)), Times.Once);
            Assert.AreEqual(1, ((List<Person>)results.Value).Count);
        }

        [TestMethod]
        public void SortByBirthday_DescendingFalse_CallSort()
        {
            _parserSorterService.Setup(x => x.Sort(It.IsAny<SortOptions>()))
                .Returns(new List<Person>() { new Person() });

            var response = _parserController.SortByBirthday();
            var results = response.Result as OkObjectResult;

            _parserSorterService.Verify(x => x.Sort(It.Is<SortOptions>(x => x.ByColumn == "DateOfBirth" && x.Descending == false)), Times.Once);
            Assert.AreEqual(1, ((List<Person>)results.Value).Count);
        }

        [TestMethod]
        public void SortByBirthday_DescendingTrue_CallSort()
        {
            _parserSorterService.Setup(x => x.Sort(It.IsAny<SortOptions>()))
                .Returns(new List<Person>() { new Person() });

            var response = _parserController.SortByBirthday(true);
            var results = response.Result as OkObjectResult;

            _parserSorterService.Verify(x => x.Sort(It.Is<SortOptions>(x => x.ByColumn == "DateOfBirth" && x.Descending == true)), Times.Once);
            Assert.AreEqual(1, ((List<Person>)results.Value).Count);
        }

        [TestMethod]
        public void SortByName_DescendingFalse_CallSort()
        {
            _parserSorterService.Setup(x => x.Sort(It.IsAny<SortOptions>()))
                .Returns(new List<Person>() { new Person() });

            var response = _parserController.SortByName();
            var results = response.Result as OkObjectResult;

            _parserSorterService.Verify(x => x.Sort(It.Is<SortOptions>(x => x.ByColumn == "firstname" && x.Descending == false)), Times.Once);
            Assert.AreEqual(1, ((List<Person>)results.Value).Count);
        }

        [TestMethod]
        public void SortByName_DescendingTrue_CallSort()
        {
            _parserSorterService.Setup(x => x.Sort(It.IsAny<SortOptions>()))
                .Returns(new List<Person>() { new Person() });

            var response = _parserController.SortByName(true);
            var results = response.Result as OkObjectResult;

            _parserSorterService.Verify(x => x.Sort(It.Is<SortOptions>(x => x.ByColumn == "firstname" && x.Descending == true)), Times.Once);
            Assert.AreEqual(1, ((List<Person>)results.Value).Count);
        }
    }
}
