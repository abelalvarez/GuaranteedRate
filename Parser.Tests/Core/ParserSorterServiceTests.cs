﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Parser.Core;
using Parser.Core.Dtos;
using Parser.Database.Context;
using System;
using System.Linq;

namespace Parser.Tests.Core
{
    [TestClass]
    public class ParserSorterServiceTests
    {
        private IParserSorterService _parseSortService;
        private Mock<ILogger<ParserSorterService>> _logger = new Mock<ILogger<ParserSorterService>>();
        private ParserContext _context;

        public ParserSorterServiceTests()
        {
            var options = new DbContextOptionsBuilder<ParserContext>()
              .UseInMemoryDatabase(databaseName: "BenefitsDB")
              .Options;

            _context = new ParserContext(options);
            _context.Database.EnsureDeleted();

            _parseSortService = new ParserSorterService(_logger.Object, _context);

        }

        #region Constructor
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ParserSorterService_NullLogger_ArgumentNullException()
        {
            _parseSortService = new ParserSorterService(null, _context);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ParserSorterService_NullContext_ArgumentNullException()
        {
            _parseSortService = new ParserSorterService(_logger.Object, null);
        }
        #endregion

        #region ParseAndSave
        [TestMethod]
        public void ParseAndSave_CommaDelimeter_Save()
        {
            _parseSortService.ParseAndSave("Alvarez  , Gabriel             , male   , Blue   		  , 5/27/2019");
            Assert.AreEqual("Gabriel", _context.Persons.FirstOrDefault().FirstName);
        }

        [TestMethod]
        public void ParseAndSave_PipeDelimeter_Save()
        {
            
            _parseSortService.ParseAndSave("Alvarez2  | Abel		 | male   | Blue   		  | 12/2/1989");
            Assert.AreEqual("Abel", _context.Persons.FirstOrDefault().FirstName);
        }

        [TestMethod]
        public void ParseAndSave_SpaceDelimeter_Save()
        {

            _parseSortService.ParseAndSave("Alvarez3    Carlos		   male     Blue   		    5/27/2015");
            Assert.AreEqual("Carlos", _context.Persons.FirstOrDefault().FirstName);
        }
        #endregion

        #region Sort

        [TestMethod]
        public void Sort_LastName_Ascending()
        {
            SeedData();
            var output = _parseSortService.Sort(new SortOptions { ByColumn = "LastName" });

            Assert.AreEqual("Lala", output[0].FirstName);
            Assert.AreEqual("Alvarez", output[0].LastName);

            Assert.AreEqual("Gabriel", output[1].FirstName);
            Assert.AreEqual("Alvarez", output[1].LastName);

            Assert.AreEqual("Margiet", output[2].FirstName);
            Assert.AreEqual("Alvarez1", output[2].LastName);

            Assert.AreEqual("Abel", output[3].FirstName);
            Assert.AreEqual("Alvarez2", output[3].LastName);

            Assert.AreEqual("Carlos", output[4].FirstName);
            Assert.AreEqual("Alvarez3", output[4].LastName);
        }

        [TestMethod]
        public void Sort_LastName_Descending()
        {
            SeedData();
            var output = _parseSortService.Sort(new SortOptions { ByColumn = "LastName", Descending = true });

            Assert.AreEqual("Carlos", output[0].FirstName);
            Assert.AreEqual("Alvarez3", output[0].LastName);

            Assert.AreEqual("Abel", output[1].FirstName);
            Assert.AreEqual("Alvarez2", output[1].LastName);

            Assert.AreEqual("Margiet", output[2].FirstName);
            Assert.AreEqual("Alvarez1", output[2].LastName);

            Assert.AreEqual("Gabriel", output[3].FirstName);
            Assert.AreEqual("Alvarez", output[3].LastName);

            Assert.AreEqual("Lala", output[4].FirstName);
            Assert.AreEqual("Alvarez", output[4].LastName);
        }

        [TestMethod]
        public void Sort_FirstNameAscending_Sorted()
        {
            SeedData();
            var output = _parseSortService.Sort(new SortOptions { ByColumn = "Firstname" });

            Assert.AreEqual("Abel", output[0].FirstName);

            Assert.AreEqual("Carlos", output[1].FirstName);

            Assert.AreEqual("Gabriel", output[2].FirstName);

            Assert.AreEqual("Lala", output[3].FirstName);

            Assert.AreEqual("Margiet", output[4].FirstName);
        }

        [TestMethod]
        public void Sort_FirstNameDescending_Sorted()
        {
            SeedData();
            var output = _parseSortService.Sort(new SortOptions { ByColumn = "Firstname", Descending = true });

            Assert.AreEqual("Margiet", output[0].FirstName);
            
            Assert.AreEqual("Lala", output[1].FirstName);
            
            Assert.AreEqual("Gabriel", output[2].FirstName);
            
            Assert.AreEqual("Carlos", output[3].FirstName);
            
            Assert.AreEqual("Abel", output[4].FirstName);
        }

        [TestMethod]
        public void Sort_GenderAscending_Sorted()
        {
            SeedData();
            var output = _parseSortService.Sort(new SortOptions { ByColumn = "Gender" });

            Assert.AreEqual("female", output[0].Gender);
            Assert.AreEqual("Alvarez", output[0].LastName);

            Assert.AreEqual("female", output[1].Gender);
            Assert.AreEqual("Alvarez1", output[1].LastName);

            Assert.AreEqual("male", output[2].Gender);
            Assert.AreEqual("Alvarez", output[2].LastName);

            Assert.AreEqual("male", output[3].Gender);
            Assert.AreEqual("Alvarez2", output[3].LastName);

            Assert.AreEqual("male", output[4].Gender);
            Assert.AreEqual("Alvarez3", output[4].LastName);
        }

        [TestMethod]
        public void Sort_GenderMaleFirstThenLastnameDescending_Sorted()
        {
            SeedData();
            var output = _parseSortService.Sort(new SortOptions { ByColumn = "Gender", Descending = true });

            Assert.AreEqual("male", output[0].Gender);
            Assert.AreEqual("Alvarez3", output[0].LastName);
            
            Assert.AreEqual("male", output[1].Gender);
            Assert.AreEqual("Alvarez2", output[1].LastName);

            Assert.AreEqual("female", output[3].Gender);
            Assert.AreEqual("Alvarez1", output[3].LastName);

            Assert.AreEqual("male", output[2].Gender);
            Assert.AreEqual("Alvarez", output[2].LastName);

            Assert.AreEqual("female", output[4].Gender);
            Assert.AreEqual("Alvarez", output[4].LastName);

        }

        [TestMethod]
        public void Sort_FavoriteColorAscending_Sorted()
        {
            SeedData();
            var output = _parseSortService.Sort(new SortOptions { ByColumn = "FavoriteColor" });

            Assert.AreEqual("Blue", output[0].FavoriteColor);
            Assert.AreEqual("Abel", output[0].FirstName);

            Assert.AreEqual("Green", output[1].FavoriteColor);
            Assert.AreEqual("Carlos", output[1].FirstName);

            Assert.AreEqual("Orange", output[2].FavoriteColor);
            Assert.AreEqual("Gabriel", output[2].FirstName);

            Assert.AreEqual("Pink", output[3].FavoriteColor);
            Assert.AreEqual("Lala", output[3].FirstName);

            Assert.AreEqual("Red", output[4].FavoriteColor);
            Assert.AreEqual("Margiet", output[4].FirstName);
        }

        [TestMethod]
        public void Sort_FavoriteColorDescending_Sorted()
        {
            SeedData();
            var output = _parseSortService.Sort(new SortOptions { ByColumn = "FavoriteColor", Descending = true });

            Assert.AreEqual("Red", output[0].FavoriteColor);
            Assert.AreEqual("Margiet", output[0].FirstName);
            
            Assert.AreEqual("Pink", output[1].FavoriteColor);
            Assert.AreEqual("Lala", output[1].FirstName);

            Assert.AreEqual("Orange", output[2].FavoriteColor);
            Assert.AreEqual("Gabriel", output[2].FirstName);

            Assert.AreEqual("Green", output[3].FavoriteColor);
            Assert.AreEqual("Carlos", output[3].FirstName);
            
            Assert.AreEqual("Blue", output[4].FavoriteColor);
            Assert.AreEqual("Abel", output[4].FirstName);
        }

        [TestMethod]
        public void Sort_DateOfBirthAscending_Sortedg()
        {
            SeedData();
            var output = _parseSortService.Sort(new SortOptions { ByColumn = "DateOfBirth" });

            Assert.AreEqual("12/2/1989", output[0].DateOfBirth.ToString("M/d/yyyy"));
            Assert.AreEqual("Abel", output[0].FirstName);

            Assert.AreEqual("5/27/1991", output[1].DateOfBirth.ToString("M/d/yyyy"));
            Assert.AreEqual("Margiet", output[1].FirstName);

            Assert.AreEqual("5/27/2015", output[2].DateOfBirth.ToString("M/d/yyyy"));
            Assert.AreEqual("Carlos", output[2].FirstName);

            Assert.AreEqual("5/27/2019", output[3].DateOfBirth.ToString("M/d/yyyy"));
            Assert.AreEqual("Gabriel", output[3].FirstName);

            Assert.AreEqual("5/27/2021", output[4].DateOfBirth.ToString("M/d/yyyy"));
            Assert.AreEqual("Lala", output[4].FirstName);
        }

        [TestMethod]
        public void Sort_DateOfBirthDescending_Sorted()
        {
            SeedData();
            var output = _parseSortService.Sort(new SortOptions { ByColumn = "DateOfBirth", Descending = true });

            Assert.AreEqual("5/27/2021", output[0].DateOfBirth.ToString("M/d/yyyy"));
            Assert.AreEqual("Lala", output[0].FirstName);

            Assert.AreEqual("5/27/2019", output[1].DateOfBirth.ToString("M/d/yyyy"));
            Assert.AreEqual("Gabriel", output[1].FirstName);

            Assert.AreEqual("5/27/2015", output[2].DateOfBirth.ToString("M/d/yyyy"));
            Assert.AreEqual("Carlos", output[2].FirstName);

            Assert.AreEqual("5/27/1991", output[3].DateOfBirth.ToString("M/d/yyyy"));
            Assert.AreEqual("Margiet", output[3].FirstName);

            Assert.AreEqual("12/2/1989", output[4].DateOfBirth.ToString("M/d/yyyy"));
            Assert.AreEqual("Abel", output[4].FirstName);
        }

        private void SeedData()
        {
            _parseSortService.ParseAndSave("Alvarez1  | Margiet		 | female   | Red   		  | 5/27/1991");
            _parseSortService.ParseAndSave("Alvarez2  | Abel		 | male   | Blue   		  | 12/2/1989");
            _parseSortService.ParseAndSave("Alvarez3    Carlos		   male     Green   		    5/27/2015");
            _parseSortService.ParseAndSave("Alvarez  | Lala		 | female   | Pink   		  | 5/27/2021");
            _parseSortService.ParseAndSave("Alvarez  , Gabriel             , male   , Orange   		  , 5/27/2019");
        }
        #endregion
    }
}
