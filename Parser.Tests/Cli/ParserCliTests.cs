﻿using Microsoft.Extensions.Logging;
using Moq;
using Parser.Core;
using ParserCli.CommandLine;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Parser.Core.Dtos;

namespace Parser.Tests.Cli
{
    [TestClass]
    public class ParserCliTests
    {
        private ParserCli.ParserCli _parserCli;
        private readonly Mock<ILogger<ParserCli.ParserCli>> _loggerMock = new Mock<ILogger<ParserCli.ParserCli>>();
        private readonly Mock<IFileLoader> _fileLoaderMock = new Mock<IFileLoader>();
        private readonly Mock<IParserSorterService> _parseSortService = new Mock<IParserSorterService>();

        public ParserCliTests()
        {
            _parserCli = new ParserCli.ParserCli(_loggerMock.Object, _fileLoaderMock.Object, _parseSortService.Object);
        }

        #region Constructor
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ParserCli_NullLogger_ArgumentNullException()
        {
            _parserCli = new ParserCli.ParserCli(null, null, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ParserCli_NullFileLoader_ArgumentNullException()
        {
            _parserCli = new ParserCli.ParserCli(_loggerMock.Object, null, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ParserCli_NullParser_ArgumentNullException()
        {
            _parserCli = new ParserCli.ParserCli(_loggerMock.Object, _fileLoaderMock.Object, null);
        }
        #endregion

        [TestMethod]
        public void StartAsync_Args_ArgumentNullException()
        {
            var response = _parserCli.StartAsync(null);

            Assert.IsNotNull(response.Exception);
        }


        [TestMethod, Ignore("Not passing is CI")]
        public void StartAsync_ValidParams_FormatOutput()
        {
            var args = new string[] { "path" };
            var output = new StringWriter();
            Console.SetOut(output);

            _parseSortService.Setup(x => x.Sort(It.IsAny<SortOptions>()))
                .Returns(new System.Collections.Generic.List<Person>());
            var response = _parserCli.StartAsync(args);

            Assert.AreEqual(output.ToString(), "LastName        FirstName       Gender     FavoriteColor   DateOfBirth\n\n\r\n");

            _parseSortService.Verify(x => x.Sort(It.IsAny<SortOptions>()), Times.Once);
            Assert.IsNull(response.Exception);
            Assert.AreEqual(1, response.Result);
        }
    }
}
