﻿using System.Threading.Tasks;

namespace ParserCli
{
    public interface IStart
    {
        Task<int> StartAsync(string[] args);
    }
}
