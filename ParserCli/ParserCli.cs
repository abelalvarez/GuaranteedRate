﻿using Microsoft.Extensions.Logging;
using Parser.Core;
using ParserCli.CommandLine;
using System;
using System.Threading.Tasks;
using Parser.Core.Dtos;
using CommandLineParser = CommandLine;
using CommandLine;
using ParserCli.Utils;

namespace ParserCli
{
    public class ParserCli : IStart
    {
        private readonly ILogger _logger;
        private readonly IFileLoader _fileLoader;
        private readonly IParserSorterService _parserSorterService;

        public ParserCli(ILogger<ParserCli> logger, IFileLoader fileLoader, IParserSorterService parseService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _fileLoader = fileLoader ?? throw new ArgumentNullException(nameof(fileLoader));
            _parserSorterService = parseService ?? throw new ArgumentNullException(nameof(parseService));
        }

        public async Task<int> StartAsync(string[] args)
        {
            return await CommandLineParser.Parser.Default.ParseArguments<CommandLineOptions>(args)
        .MapResult(async (CommandLineOptions opts) =>
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(opts.Path))
                {
                    var input = _fileLoader.Load(opts.Path);
                    _parserSorterService.ParseAndSave(input);
                }

                var sorted = _parserSorterService.Sort(new SortOptions { Descending = opts.SortDescending, ByColumn = opts.Sort });
                Console.WriteLine(sorted.ToFormat());
                return 1;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogCritical(ex.Message);
                Console.WriteLine(ex.Message);
                return -3; // Unhandled error
            }
        },
        errs => Task.FromResult(-1)); // Invalid arguments
        }
    }
}
