﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Parser.Core;
using Parser.Database.Context;
using ParserCli.CommandLine;
using System;

namespace ParserCli
{
    class Program
    {
        static void Main(string[] args)
        {
            var services = new ServiceCollection();
            ConfigureServices(services);

            ServiceProvider serviceProvider = services.BuildServiceProvider();

            var logger = serviceProvider
                .GetService<ILoggerFactory>()
                .CreateLogger<Program>();

            logger.LogDebug("Starting application");

            var startup = serviceProvider.GetService<IStart>();
            startup.StartAsync(args);
        }

        private static void ConfigureServices(ServiceCollection services)
        {
            services.AddLogging(config =>
            {
                config.SetMinimumLevel(LogLevel.Error);
                config.AddConsole();
            })
                .AddSingleton<IFileLoader, FileLoader>()
                .AddScoped<IStart, ParserCli>()
                .AddTransient<IParserSorterService, ParserSorterService>()
                .AddDbContext<ParserContext>(options =>
                    options.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=Parser;Trusted_Connection=True;MultipleActiveResultSets=true"))
                ;
            ;
        }
    }
}
