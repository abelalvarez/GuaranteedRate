﻿using Parser.Core.Dtos;
using System;
using System.Collections.Generic;

namespace ParserCli.Utils
{
    public static class Extensions
    {
        public static string ToFormat(this List<Person> people)
        {
            String output = String.Format("{0,-15} {1,-15} {2,-10} {3,-15} {4,-10}\n\n", "LastName", "FirstName", "Gender", "FavoriteColor", "DateOfBirth");
            foreach (var person in people)
            {
                output += String.Format("{0,-15} {1,-15} {2,-10} {3,-15} {4,-10}\n",
                      person.LastName, person.FirstName, person.Gender, person.FavoriteColor, person.DateOfBirth.ToString("M/d/yyyy"));
            }
            return output;
        }
    }
}
