﻿namespace ParserCli.CommandLine
{
    public interface IFileLoader
    {
        string Load(string path);
    }
}
