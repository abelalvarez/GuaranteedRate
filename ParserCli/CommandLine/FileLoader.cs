﻿using Microsoft.Extensions.Logging;
using System;
using System.IO;

namespace ParserCli.CommandLine
{
    public class FileLoader : IFileLoader
    {
        private readonly ILogger _logger;
        public FileLoader(ILogger<FileLoader> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public string Load(string path)
        {
            _logger.LogDebug($"Starting reading file from path {path}");
            var reader = File.ReadAllText(path);
            return reader;
        }
    }
}
