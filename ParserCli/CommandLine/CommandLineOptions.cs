﻿using CommandLine;

namespace ParserCli.CommandLine
{
    public class CommandLineOptions
    {
        [Option(shortName: 'p', longName:"path", Required = false, HelpText = "File Path to analyze.", Default = "")]
        public string Path { get; set; }

        [Option(shortName: 's', longName: "sort", Required = false, HelpText = "Sort column", Default = "gender")]
        public string Sort { get; set; }

        [Option(shortName: 'd', longName: "descending", Required = false, HelpText = "Sort descending", Default = false)]
        public bool SortDescending { get; set; }
    }
}
