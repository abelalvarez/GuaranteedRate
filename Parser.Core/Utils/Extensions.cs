﻿using Parser.Core.Dtos;
using System.Collections.Generic;

namespace Parser.Core.Utils
{
    public static class Extensions
    {
        public static Database.Models.Person ToDbPerson(this Person person)
        {
            return new Database.Models.Person
            {
                DateOfBirth = person.DateOfBirth,
                FavoriteColor = person.FavoriteColor,
                FirstName = person.FirstName,
                Gender = person.Gender,
                LastName = person.LastName,
            };
        }

        public static IEnumerable<Database.Models.Person> ToDbPeople(this IEnumerable<Person> people)
        {
            foreach(var person in people)
            {
                yield return ToDbPerson(person);
            }
        }

        public static Person ToDtoPerson(this Database.Models.Person person)
        {
            return new Person
            {
                DateOfBirth = person.DateOfBirth,
                FavoriteColor = person.FavoriteColor,
                FirstName = person.FirstName,
                Gender = person.Gender,
                LastName = person.LastName,
            };
        }

        public static IEnumerable<Person> ToDtoPeople(this IEnumerable<Database.Models.Person> people)
        {
            foreach (var person in people)
            {
                yield return ToDtoPerson(person);
            }
        }
    }
}
