﻿namespace Parser.Core.Dtos
{
    public class SortOptions
    {
        public string ByColumn { get; set; }
        public bool Descending { get; set; }
    }
}
