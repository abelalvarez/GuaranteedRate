﻿using System;
using System.Dynamic;

namespace Parser.Core.Dtos
{
    public class Person : DynamicObject
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string FavoriteColor { get; set; }
        public DateTime DateOfBirth { get; set; }
    }
}
