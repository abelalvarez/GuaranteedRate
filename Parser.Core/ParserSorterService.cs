﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Parser.Core.Dtos;
using Parser.Core.Utils;
using Parser.Database.Context;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Linq.Dynamic.Core;

namespace Parser.Core
{
    public class ParserSorterService : IParserSorterService
    {
        private readonly ILogger _logger;
        private readonly ParserContext _context;

        public ParserSorterService(ILogger<ParserSorterService> logger, ParserContext context)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public IEnumerable<Person> ParseAndSave(string input)
        {
            var rows = Regex.Split(input, "[\r\n]+");

            var results = rows.Where(x => !string.IsNullOrEmpty(x))
                .Select(x => Regex.Split(x, "[\\s\\|\\,]+"))
                .ToList();

            var headers = new string[] { "LastName", "FirstName", "Gender", "FavoriteColor", "DateOfBirth" };
            var output = results.Select(x => CreatePerson(headers, x));

            _context.Persons.AddRange(output.ToDbPeople());
            _context.SaveChanges();

            return output;
        }

        public List<Person> Sort(SortOptions options)
        {
            var sortColumn = GetPropValue(new Person(), options.ByColumn);
            var query = _context.Persons
                .OrderBy(sortColumn)
                .ThenBy(x => x.LastName);

            var result = options.Descending
                ? (query.Reverse())
                : (query);

            return result.ToDtoPeople().ToList();
        }

        private string GetPropValue(object obj, string propName)
        {
            try
            {
                var property = obj.GetType().GetProperties()
                    .Where(x => string.Equals(x.Name, propName, StringComparison.OrdinalIgnoreCase))
                    .Select(x => x.Name)
                    .First();

                return property;
            }
            catch (Exception ex)
            {
                throw new Exception("Sort command does not match any existing column", ex);
            }
        }

        private Person CreatePerson(string[] headers, string[] row)
        {
            var expandoObj = new ExpandoObject();
            var expandoObjCollection = (ICollection<KeyValuePair<String, Object>>)expandoObj;

            for (var i = 0; i < headers.Length; i++)
            {
                expandoObjCollection.Add(new KeyValuePair<string, object>(headers[i], row[i]));
            }

            var serialized = JsonConvert.SerializeObject(expandoObj);
            var deserialized = JsonConvert.DeserializeObject<Person>(serialized);

            return deserialized;
        }

    }
}
