﻿using Parser.Core.Dtos;
using System.Collections.Generic;

namespace Parser.Core
{
    public interface IParserSorterService
    {
        IEnumerable<Person> ParseAndSave(string input);
        List<Person> Sort(SortOptions options);
    }
}
